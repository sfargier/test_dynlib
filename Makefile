
EXE  = test
SRC  = test.c
OBJS = ${SRC:.c=.o}
CXXFLAGS = -fPIC -std=c++11
LDFLAGS = -fPIC -std=c++11

all: test libtest.so

libtest.so: entry_point.o foo.o
	# ${CXX} ${LDFLAGS} -shared $^ -o $@
	${CXX} ${LDFLAGS} -lpthread -shared $^ -o $@

test: test.o
	${CXX} ${LDFLAGS} $^ -ldl -lpthread -o $@


.cpp.o:
	${CXX} ${CXXFLAGS} -c $< -o $@

clean:
	@rm -rf *.o *.so *.a test

.PHONY: clean
