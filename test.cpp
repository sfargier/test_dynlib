
#include <dlfcn.h>
#include <iostream>
#include <unistd.h>

typedef void (*fun_t)();

int main() {

  // /* MITIGATION 3, pre-open the lib and flag as NODELETE (see README.md) */
  void *lib2 = dlopen("./libtest.so", RTLD_LOCAL | RTLD_LAZY | RTLD_NODELETE);
  if (!lib2) {
    std::cerr << "failed to dlopen: " << dlerror() << std::endl;
    _exit(1);
  }
  dlclose(lib2);

  void *lib = dlopen("./libtest.so", RTLD_LOCAL | RTLD_LAZY);
  if (!lib) {
    std::cerr << "failed to dlopen: " << dlerror() << std::endl;
    _exit(1);
  }

  fun_t f = reinterpret_cast<fun_t>(dlsym(lib, "entry_point"));
  if (!f) {
    std::cerr << "failed to get entry_point" << std::endl;
    _exit(1);
  }

  f();
  std::cerr << "function called" << std::endl;

  dlclose(lib);

  std::cerr << "lib closed" << std::endl;

  return 0;
}