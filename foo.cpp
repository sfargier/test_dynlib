
#include "foo.hpp"
#include <iostream>
#include <string>

class Bar {
public:
  Bar() : data("test") { std::cerr << "Bar(" << this << ")" << std::endl; }
  ~Bar() { std::cerr << "~Bar(" << this << ")" << std::endl; }

  std::string data;
};

static thread_local Bar bar;
static Bar bar2;

Foo::Foo() {
  (void) bar;
  std::cerr << "Foo(" << this << ") "
            << "Bar(" << ((void*) &bar) << ") "
            << "Bar2(" << ((void*) &bar2) << ")" << std::endl;
}
Foo::~Foo() {
  std::cerr << "~Foo(" << this << ") "
            << "Bar(" << ((void*) &bar) << ")"
            << "Bar2(" << ((void*) &bar2) << ")" << std::endl;
}
