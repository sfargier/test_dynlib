
#include "foo.hpp"
#include <iostream>

extern "C" {
void entry_point() {
  std::cerr << "entry_point called" << std::endl;
  Foo f;
}
}