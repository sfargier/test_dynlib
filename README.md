# dlopen and thread_local

There seem to be an issue with dlopen and [thread_local](https://en.cppreference.com/w/cpp/language/storage_duration) storage on CentOS7.

This project creates an app [test](./test.cpp) that dynamically loads [libtest](./entry_point.cpp) which uses `thread_local` storage.

On recent distros (CentOS Stream9 and Arch-Linux) having a `thread_local`
variable seems to automatically activate "RTLD_NODELETE", delaying statics and global deletion to the process end (at least for those flagged as thread_local).

On CentOS7 mapped memory is released on `dlclose`, still the `thread_local`
variable is accessed on thread stop (process exit), accessing already released
memory and generating a segfault.

Proposed mitigation:
- do not use `thread_local` (even tho it's really convenient and efficient).
- use `RTLD_NODELETE` on loaded libraries.
- dlopen the library with `RTLD_NODELETE` before real open (close it just after).
    - it seems that glibc-2.17 won't modify flags of already opened libraries (fixed in later versions)

From glibc (see link 1):
> The compiler can handle constructors, but destructors need more runtime context. It could be possible that a dynamically loaded library defines and constructs a thread_local variable, but is dlclose()'d before thread exit. The destructor of this variable will then have the rug pulled from under it and crash. As a result, the dynamic linker needs to be informed so as to not unload the DSO.

Related info:
1. [glibc: Destructor support for thread_local variables](https://sourceware.org/glibc/wiki/Destructor%20support%20for%20thread_local%20variables)
2. [Blog: all about thread local storage](http://maskray.me/blog/2021-02-14-all-about-thread-local-storage)
3. [glibc-2.17 (CC7) source](https://github.com/bminor/glibc/blob/glibc-2.17/elf/dl-open.c)

